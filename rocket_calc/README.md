# RocketCalc

**TODO: Add description**
Houston, greetings from Kyiv! You're looking at app, designed to perform calculations, necessary for estimating,
how much fuel does the ship need to perform the operation of multiple actions:
in actuality, it can launch from a specific planet, and can land to another!
And since we don't know whether we can get fuel out there, the calculations take into account that we will stock the fuel once,
before the space journey even begins!

to run the app's code, type in the command in the main directory

```
>iex -S mix
```
you can also run the tests by using following command in the command line

```
>mix test
```

once the app starts, usable function includes the following:

```
iex> RocketCalc.calculate_total_fuel(ship_mass, directives)
```

Note, that the function accepts 2 arguements: an integer number, that describes ship's mass
as well as the directives, that are formated via the following principle:

```
[{:launch, 4.2}, {:land, 6.9}, ...]
```

Specifically, 2 variations of atoms as the command, :launch or :land, and a floating point number, larger than 0,
since it's the gravity we're talking about. They are bundled in a tuple, and since we can get a lot of directives,
it should be contained in a list with other directive tuples...
