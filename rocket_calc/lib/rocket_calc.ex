defmodule RocketCalc do
  @moduledoc """
  Documentation for `RocketCalc`.
  """


  @doc """
  based on ship's mass, and the directive it will have to proceed, calculates
  the amount of fuel needed to perform all of them without need of refueling
  initially, the method reverses the directives order because for proper estimations
  we need to start from the end of the cycle

  ## Examples

      iex> RocketCalc.calculate_total_fuel(28801, [{:launch, 9.807}, {:land, 1.62}, {:launch, 1.62}, {:land, 9.807}])
      51898

  """
  def calculate_total_fuel(_ship_mass, []) do {:error, "Houston, we have a problem: no directives"} end

  def calculate_total_fuel(ship_mass, _directives) when not Kernel.is_number(ship_mass) or ship_mass <= 0 do {:error, "Houston, we have a problem: invalid mass"} end

  def calculate_total_fuel(ship_mass, directives) do
    case validate_directives_format(directives) do
      :true -> calculate_loaded_mass(ship_mass, Enum.reverse(directives)) - ship_mass
      :false -> {:error, "Houston, we have a problem: invalid directives format, list of tuples {:launch, data} or {:land, data}, please "}
    end
  end

  defp validate_directives_format(directives) do
    case directives do
      [{command, data} | tail] when command in [:launch, :land] and data > 0 -> validate_directives_format(tail)
      [] -> :true
      _ -> :false
    end
  end

  # calculates the mass of the ship loaded with fuel altogether
  defp calculate_loaded_mass(mass, []) do
    mass
  end

  defp calculate_loaded_mass(mass, [head | tail]) do
    fuel = calculate_fuel_needed(mass, head)
    calculate_loaded_mass(mass+fuel, tail)
  end

  # calculates fuel needed to perform a provided directive
  defp calculate_fuel_needed(mass, directive) do
    fuel = calculate_fuel(mass, directive)
    fuel_for_fuel = calculate_additional_fuel(fuel, directive)
    fuel+fuel_for_fuel
  end

  #calculate fuel needed to transport some mass according to formulas provided

  defp calculate_fuel(mass, {:launch, gravity}) do
      Kernel.trunc(mass*gravity*0.042 - 33)
  end

  defp calculate_fuel(mass, {:land, gravity}) do
    Kernel.trunc(mass*gravity*0.033 - 42)
  end

  # calculate fuel needed to transport fuel itself
  defp calculate_additional_fuel(fuel, directive, acc \\ 0)

  defp calculate_additional_fuel(fuel, _directive, acc) when fuel <= 0 do
    acc - fuel
  end

  defp calculate_additional_fuel(fuel, directive, acc) do
    fuel_for_fuel = calculate_fuel(fuel, directive)
    calculate_additional_fuel(fuel_for_fuel, directive, acc+fuel_for_fuel)
  end

end
