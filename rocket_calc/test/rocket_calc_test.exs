defmodule RocketCalcTest do
  use ExUnit.Case
  doctest RocketCalc

  describe "RocketCalc.calculate_total_fuel/2" do

    test "To the Moon and back" do
      assert RocketCalc.calculate_total_fuel(28801, [{:launch, 9.807},
       {:land, 1.62}, {:launch, 1.62}, {:land, 9.807}]) == 51898
    end

    test "Martian expedition" do
      assert RocketCalc.calculate_total_fuel(14606, [{:launch, 9.807},
       {:land, 3.711}, {:launch, 3.711}, {:land, 9.807}]) == 33388
    end

    test "Road trip" do
      assert RocketCalc.calculate_total_fuel(75432, [{:launch, 9.807},
       {:land, 1.62}, {:launch, 1.62},
       {:land, 3.711}, {:launch, 3.711}, {:land, 9.807}]) == 212161
    end

    test "Invalid mass" do
      assert RocketCalc.calculate_total_fuel(-75432, [{:launch, 9.807},
       {:land, 1.62}, {:launch, 1.62},
       {:land, 3.711}, {:launch, 3.711}, {:land, 9.807}]) == {:error, "Houston, we have a problem: invalid mass"}
       assert RocketCalc.calculate_total_fuel("data here", [{:launch, 9.807},
        {:land, 1.62}, {:launch, 1.62},
        {:land, 3.711}, {:launch, 3.711}, {:land, 9.807}]) == {:error, "Houston, we have a problem: invalid mass"}
    end

    test "No directives" do
      assert RocketCalc.calculate_total_fuel(75432, []) == {:error, "Houston, we have a problem: no directives"}
    end

    test "Invalid directives format" do
      assert RocketCalc.calculate_total_fuel(75432,
      [[:launch, 9.807], [:land, 1.62],
      [:launch, 1.62], [:land, 3.711],
      [:launch, 3.711], [:land, 9.807]]) == {:error, "Houston, we have a problem: invalid directives format, list of tuples {:launch, data} or {:land, data}, please "}
    end
  end

end
